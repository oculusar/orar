#include "ScreenQuad.h"

#ifndef OVR_DEBUG_LOG
#define OVR_DEBUG_LOG(x)
#endif

#define M_PI           3.14159265358979323846

struct Vertex
{
	Vector2f  Pos;
	Vector2f     UV;
};

ScreenQuad::ScreenQuad()
{
	rectifiedShader.load("VertexShader.glsl", "RectificationFragmentShader.glsl");
	string errRect;
	if (!rectifiedShader.compile(&errRect)) {
		throw new Exception();
	}
	simpleShader.load("VertexShader.glsl", "SimpleFragmentShader.glsl");
	string errSimp;
	if (!simpleShader.compile(&errSimp)) {
		throw new Exception();
	}
	//-----

	//Create Model
	Vertex corners[] =
	{
		Vertex{ Vector2f(-1,1), Vector2f(0,0)},
		Vertex{ Vector2f(1,1),Vector2f(1,0)},
		Vertex{ Vector2f(1,-1),Vector2f(1,1)},
		Vertex{ Vector2f(-1,-1),Vector2f(0,1)}
	};

	vbuffer = new VertexBuffer(corners, 4*sizeof(Vertex));
	
}



ScreenQuad::~ScreenQuad()
{
	delete vbuffer; vbuffer = nullptr;
}


void ScreenQuad::render(GLuint texId, bool rectified)
{
	ShaderProgram* toUse;
	if(rectified)
	{
		toUse = &rectifiedShader;
	} else
	{
		toUse = &simpleShader;
	}

	toUse->activate();

	toUse->setParameter(toUse->getParameterID("_MainTex"), 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texId);

	glBindBuffer(GL_ARRAY_BUFFER, vbuffer->buffer);

	//TODO move attributes to shader program
	GLint posLoc = glGetAttribLocation(toUse->m_ShaderProgram, "Position");
	GLint uvLoc = glGetAttribLocation(toUse->m_ShaderProgram, "TexCoord");

	glEnableVertexAttribArray(posLoc);
	glEnableVertexAttribArray(uvLoc);

	glVertexAttribPointer(posLoc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)OVR_OFFSETOF(Vertex, Pos));
	glVertexAttribPointer(uvLoc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)OVR_OFFSETOF(Vertex, UV));

	glDrawArrays(GL_QUADS, 0, 4);

	glDisableVertexAttribArray(posLoc);
	glDisableVertexAttribArray(uvLoc);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//ToTest: unbind texture

	toUse->deactivate();
}

void ScreenQuad::updateRectifyData(StereoCalibration::calib_data* data, bool left, int fw, int fh, int orw, int orh, float fov) { //TODO check if the imgw and h are the correct ones
	StereoCalibration::calib_data toUse;
	if (left) {
		toUse = data[0];
	}
	else {
		toUse = data[1];
	}

	rectifiedShader.activate(); //This function gets only called when the rectification data is available.
	//We don't need to switch here

	rectifiedShader.setParameter(rectifiedShader.getParameterID("_FlycamSize"), static_cast<float>(fw), static_cast<float>(fh));
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_OR_Size"), static_cast<float>(orw), static_cast<float>(orh));
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_FX"),toUse.fx);
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_FY"),toUse.fy);
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_CX"),toUse.cx);
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_CY"),toUse.cy);
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_K1"),toUse.k1);
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_K2"),toUse.k2);

	rectifiedShader.setParameter(rectifiedShader.getParameterID("_R11"),toUse.r11);
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_R12"),toUse.r12);
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_R13"),toUse.r13);

	rectifiedShader.setParameter(rectifiedShader.getParameterID("_R21"), toUse.r21);
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_R22"), toUse.r22);
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_R23"), toUse.r23);

	rectifiedShader.setParameter(rectifiedShader.getParameterID("_R31"), toUse.r31);
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_R32"), toUse.r32);
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_R33"), toUse.r33);

	float f = (float) ((orh / 2) / tan((fov * M_PI / 180)  / 2)); //Oculus focal length
	float cx = orw / 2.f;
	float cy = orh / 2.f;

	rectifiedShader.setParameter(rectifiedShader.getParameterID("_OR_F"), f);
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_OR_CX"), cx);
	rectifiedShader.setParameter(rectifiedShader.getParameterID("_OR_CY"), cy);

	rectifiedShader.deactivate();
}