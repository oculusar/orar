//
//  ShaderProgram.h
//  Praktikum6
//
//  Created by Boss, Mark on 27.05.15.
//  Copyright (c) 2015 Boss, Mark. All rights reserved.
//

#ifndef __Praktikum6__ShaderProgram__
#define __Praktikum6__ShaderProgram__

#include <GL/CAPI_GLE.h>
#include "Win32_GLAppUtil.h"
#include <fstream>

class ShaderProgram {
public:
	GLuint m_VertexShader;
	GLuint m_FragmentShader;
	GLuint m_ShaderProgram;

    ShaderProgram();
    ~ShaderProgram();
    bool load(const char* VertexShader, const char* FragmentShader);
    bool loadVertexShader(const char* VertexShader);
    bool loadFragmentShader(const char* FragmentShader);
    bool compile(std::string* CompileErrors=NULL);
    
    GLint getParameterID(const char* ParameterName) const;
    
    void setParameter(GLint ID, float Param);
    void setParameter(GLint ID, float Param, float Param2);
    void setParameter(GLint ID, int Param);
    
    void activate() const;
    void deactivate() const;
private:
    void unloadShader(GLchar** ShaderSource);
    bool loadShader(const char* filename, GLchar** ShaderSource, unsigned long* len);
    unsigned long getFileLength(std::ifstream& file);
    
};

#endif /* defined(__Praktikum6__ShaderProgram__) */
