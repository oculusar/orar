// StereoCalibration.cpp : Defines the exported functions for the DLL application.
//

#include "StereoCalibration.h"
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <stdlib.h>

bool StereoCalibration::readStringList(const string& filename, vector<string>& l)
{
	l.resize(0);
	FileStorage fs(filename, FileStorage::READ);
	if (!fs.isOpened())
		return false;
	FileNode n = fs.getFirstTopLevelNode();
	if (n.type() != FileNode::SEQ)
		return false;
	FileNodeIterator it = n.begin(), it_end = n.end();
	for (; it != it_end; ++it)
		l.push_back((string)*it);
	return true;
}

bool StereoCalibration::writeStringList(const string& filename, vector<string> l)
{
	FileStorage fs(filename, FileStorage::WRITE);
	if (!fs.isOpened())
		return false;

	fs << "imagelist" << "[";
	for(int i = 0; i < l.size(); i++)
	{
		fs << l[i];
	}
	fs << "]";
	fs.release();

	return true;
}

Mat StereoCalibration::displayMode(Mat img) const
{
	String modeString = "DETECTING";
	if (mode == CAPTURING) {
		modeString = "CAPTURING";
	}
	else if (mode == CALIBRATING) {
		modeString = "CALIBRATED";
	}
	putText(img, modeString, Point(50, 50), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0), 2);
	return img;
}

bool StereoCalibration::findChessboardCornersAndDraw(Mat inputLeft, Mat inputRight) {
	_leftOri = inputLeft;
	_rightOri = inputRight;
	auto foundLeft = false, foundRight = false;
	cvtColor(inputLeft, inputLeft, COLOR_BGR2GRAY);
	cvtColor(inputRight, inputRight, COLOR_BGR2GRAY);
	foundLeft = findChessboardCorners(inputLeft, boardSize, cornersLeft, CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE
		+ CALIB_CB_FAST_CHECK);
	foundRight = findChessboardCorners(inputRight, boardSize, cornersRight, CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE
		+ CALIB_CB_FAST_CHECK);
	drawChessboardCorners(_leftOri, boardSize, cornersLeft, foundLeft);
	drawChessboardCorners(_rightOri, boardSize, cornersRight, foundRight);
	_leftOri = displayMode(_leftOri);
	_rightOri = displayMode(_rightOri);
	displayImages();
	if (foundLeft && foundRight) {
		return true;
	}
	else {
		return false;
	}
}

void StereoCalibration::displayImages() const
{
	Mat outl, outr;
	Size inSize = _leftOri.size();
	Size size(inSize.width / 3, inSize.height / 3);

	resize(_leftOri, outl, size);
	resize(_rightOri, outr, size);

	imshow("Left Image", outl);
	imshow("Right Image", outr);
}

void StereoCalibration::saveImages(Mat leftImage, Mat rightImage, int pairIndex, bool quickSave = false) {
	if (!quickSave) {
		cornerSubPix(leftImage, cornersLeft, Size(11, 11), Size(-1, -1),
			TermCriteria(TermCriteria::COUNT + TermCriteria::EPS,
				30, 0.01));
		cornerSubPix(rightImage, cornersRight, Size(11, 11), Size(-1, -1),
			TermCriteria(TermCriteria::COUNT + TermCriteria::EPS,
				30, 0.01));
	}
	cameraImagePoints[0].push_back(cornersLeft);
	cameraImagePoints[1].push_back(cornersRight);
	if (captureImages) {
		cvtColor(leftImage, leftImage, COLOR_BGR2GRAY);
		cvtColor(rightImage, rightImage, COLOR_BGR2GRAY);
		std::ostringstream leftString, rightString;
		leftString << dir << "/" << prefixLeft << pairIndex << "." << postfix;
		rightString << dir << "/" << prefixRight << pairIndex << "." << postfix;
		imwrite(leftString.str().c_str(), leftImage);
		imwrite(rightString.str().c_str(), rightImage);
		noOfStereoPairs++;
	}
}

void StereoCalibration::executeCalibration(Size imageSize) {
	float checkerSize = 0.25f;
	vector<vector<Point3f> > objectPoints;
	objectPoints.resize(noOfStereoPairs);
	for (auto i = 0; i<noOfStereoPairs; i++) {
		for (auto j = 0; j<boardSize.height; j++) {
			for (auto k = 0; k<boardSize.width; k++) {
				objectPoints[i].push_back(Point3f(float(j*checkerSize), float(k*checkerSize), 0.0));
			}
		}
	}
	Mat cameraMatrix[2], distCoeffs[2];
	cameraMatrix[0] = initCameraMatrix2D(objectPoints, cameraImagePoints[0], imageSize, 0);
	cameraMatrix[1] = initCameraMatrix2D(objectPoints, cameraImagePoints[1], imageSize, 0);
	Mat R, T, E, F;
	auto rms = stereoCalibrate(objectPoints, cameraImagePoints[0], cameraImagePoints[1],
		cameraMatrix[0], distCoeffs[0],
		cameraMatrix[1], distCoeffs[1],
		imageSize, R, T, E, F,
		CALIB_FIX_ASPECT_RATIO +
		CALIB_ZERO_TANGENT_DIST +
		CALIB_USE_INTRINSIC_GUESS +
		CALIB_SAME_FOCAL_LENGTH +
		CALIB_RATIONAL_MODEL +
		CALIB_FIX_K3 + CALIB_FIX_K4 + CALIB_FIX_K5 + CALIB_FIX_K6,
		TermCriteria(TermCriteria::COUNT + TermCriteria::EPS, 100, 1e-5));
	cout << "RMS Error: " << rms << "\n";
	double err = 0;
	auto npoints = 0;
	vector<Vec3f> lines[2];
	for (auto i = 0; i < noOfStereoPairs; i++)
	{
		auto npt = static_cast<int>(cameraImagePoints[0][i].size());
		Mat imgpt[2];
		for (auto k = 0; k < 2; k++)
		{
			imgpt[k] = Mat(cameraImagePoints[k][i]);
			undistortPoints(imgpt[k], imgpt[k], cameraMatrix[k], distCoeffs[k], Mat(), cameraMatrix[k]);
			computeCorrespondEpilines(imgpt[k], k + 1, F, lines[k]);
		}
		for (auto j = 0; j < npt; j++)
		{
			double errij = fabs(cameraImagePoints[0][i][j].x*lines[1][j][0] +
				cameraImagePoints[0][i][j].y*lines[1][j][1] + lines[1][j][2]) +
				fabs(cameraImagePoints[1][i][j].x*lines[0][j][0] +
				cameraImagePoints[1][i][j].y*lines[0][j][1] + lines[0][j][2]);
			err += errij;
		}
		npoints += npt;
	}
	cout << "Average Reprojection Error: " << err / npoints << endl;

	FileStorage fs("intrinsics.yml", FileStorage::WRITE);
	if (fs.isOpened()) {
		fs << "M1" << cameraMatrix[0] << "D1" << distCoeffs[0] <<
			"M2" << cameraMatrix[1] << "D2" << distCoeffs[1];
		fs.release();
	}
	else
		cout << "Error: Could not open intrinsics file.";

	Mat R1, R2, P1, P2, Q;
	Rect validROI[2];

	stereoRectify(cameraMatrix[0], distCoeffs[0], cameraMatrix[1], distCoeffs[1], imageSize, R, T, R1, R2, P1, P2, Q, CALIB_ZERO_DISPARITY, 1, imageSize, &validROI[0], &validROI[1]);
	fs.open("extrinsics.yml", FileStorage::WRITE);
	if (fs.isOpened()) {
		fs << "R" << R << "T" << T << "R1" << R1 << "R2" << R2 << "P1" << P1 << "P2" << P2 << "Q" << Q;
		fs.release();
	}
	else
		cout << "Error: Could not open extrinsics file";
}



StereoCalibration::calib_data* StereoCalibration::readCalibrationData() {
	FileStorage fs("intrinsics.yml", FileStorage::READ);
	FileStorage fsExt("extrinsics.yml", FileStorage::READ);

	calib_data left;
	calib_data right;
	if (fs.isOpened()) {
		Mat distort1, distort2, m1, m2, r1, r2;
		fsExt["R1"] >> r1;
		fs["D1"] >> distort1;
		fs["M1"] >> m1;

		left.k1 = (float)distort1.at<double>(0, 0);
		left.k2 = (float)distort1.at<double>(0, 1);

		left.fx = (float)m1.at<double>(0, 0);
		left.fy = (float)m1.at<double>(1, 1);
		left.cx = (float)m1.at<double>(0, 2);
		left.cy = (float)m1.at<double>(1, 2);

		left.r11 = (float)r1.at<double>(0, 0);
		left.r12 = (float)r1.at<double>(0, 1);
		left.r13 = (float)r1.at<double>(0, 2);

		left.r21 = (float)r1.at<double>(1, 0);
		left.r22 = (float)r1.at<double>(1, 1);
		left.r23 = (float)r1.at<double>(1, 2);

		left.r31 = (float)r1.at<double>(2, 0);
		left.r32 = (float)r1.at<double>(2, 1);
		left.r33 = (float)r1.at<double>(2, 2);
		left.rot = r1;


		fsExt["R2"] >> r2;
		fs["D2"] >> distort2;
		fs["M2"] >> m2;
		right.k1 = (float)distort2.at<double>(0, 0);
		right.k2 = (float)distort2.at<double>(0, 1);

		right.fx = (float)m2.at<double>(0, 0);
		right.fy = (float)m2.at<double>(1, 1);
		right.cx = (float)m2.at<double>(0, 2);
		right.cy = (float)m2.at<double>(1, 2);

		right.r11 = (float)r2.at<double>(0, 0);
		right.r12 = (float)r2.at<double>(0, 1);
		right.r13 = (float)r2.at<double>(0, 2);

		right.r21 = (float)r2.at<double>(1, 0);
		right.r22 = (float)r2.at<double>(1, 1);
		right.r23 = (float)r2.at<double>(1, 2);

		right.r31 = (float)r2.at<double>(2, 0);
		right.r32 = (float)r2.at<double>(2, 1);
		right.r33 = (float)r2.at<double>(2, 2);
		right.rot = r2;

		fs.release();
		fsExt.release();

		calib_data* ret = new calib_data[2];
		ret[0] = left;
		ret[1] = right;

		return ret;
	}

	return nullptr;
}

bool StereoCalibration::checkNewFrames(fc2Image &img1, fc2Image &img2) {
	auto rowBytesL = static_cast<unsigned int>(round(static_cast<double>(img1.dataSize) / static_cast<double>(img1.rows)));
	auto rowBytesR = static_cast<unsigned int>(round(static_cast<double>(img2.dataSize) / static_cast<double>(img2.rows)));

	auto inputLeft = Mat(img1.rows, img1.cols, CV_8UC3, img1.pData, rowBytesL);
	auto inputRight = Mat(img2.rows, img2.cols, CV_8UC3, img2.pData, rowBytesR);
	Mat copyImageLeft, copyImageRight;

	inputLeft.copyTo(copyImageLeft);
	inputRight.copyTo(copyImageRight);

	auto foundCornersInBothImage = false;
	inputSize = inputLeft.size();
		
	if ((inputLeft.rows != inputRight.rows) || (inputLeft.cols != inputRight.cols)) {
		cout << "Error: Images from both cameras are not of some size. Please check the size of each camera.\n";
		exit(-1);
	}
	foundCornersInBothImage = findChessboardCornersAndDraw(copyImageLeft, copyImageRight);
	
	displayImages(); 
	
	if (foundCornersInBothImage) {
		saveImages(inputLeft, inputRight, ++stereoPairIndex, true);
		return true;
	}
	
	return false;
}

string StereoCalibration::getNumberOfConfiguredImages() const
{
	ostringstream num;
	num << noOfStereoPairs;
	return num.str();
}


void StereoCalibration::startCalibration()
{
	vector<string> list;
	list.resize(noOfStereoPairs * 2);
	for(int i = 1; i <= noOfStereoPairs; i++)
	{
		ostringstream imgIndex;
		imgIndex << i;
		list.push_back(prefixLeft + imgIndex.str() + "." + postfix);
		list.push_back(prefixRight + imgIndex.str() + "." + postfix);
	}
	writeStringList("stereo_calib.xml", list);
	calibrateFromSavedImages();
}

void StereoCalibration::calibrateFromSavedImages() {
	Size imageSize;

	vector<string> imagelist;
	bool ok = readStringList(imagelist_file_name, imagelist);
	if (!ok || imagelist.empty())
	{
		cout << "can not open " << imagelist_file_name << " or the string list is empty" << endl;
		return;
	}

	if (imagelist.size() % 2 != 0)
	{
		cout << "Error: the image list contains odd (non-even) number of elements\n";
		return;
	}
	int nimages = (int)imagelist.size() / 2;
	for (int i = 0; i < nimages; i++)
	{
		const string& filenameLeft = imagelist[i * 2 + 0];
		const string& filenameRight = imagelist[i * 2 + 1];

		Mat inputLeft, inputRight, copyImageLeft, copyImageRight;

		inputLeft = imread(filenameLeft, 0);
		inputRight = imread(filenameRight, 0);

		
		if (inputLeft.empty() || inputRight.empty()) {
			cout << "\nCould no find image: " << filenameLeft << " or " << filenameRight << ". Skipping images.\n";
			continue;
		}
		imageSize = inputLeft.size();

		if ((inputLeft.rows != inputRight.rows) || (inputLeft.cols != inputRight.cols)) {
			cout << "\nError: Left and Right images are not of some size. Please check the size of the images. Skipping Images.\n";
			continue;
		}

		inputLeft.copyTo(copyImageLeft);
		inputRight.copyTo(copyImageRight);

		auto foundCornersInBothImage = findChessboardCornersAndDraw(inputLeft, inputRight);
		if (foundCornersInBothImage && stereoPairIndex<noOfStereoPairs) {
			saveImages(copyImageLeft, copyImageRight, ++stereoPairIndex);
		}
		displayImages();
	}
	if (stereoPairIndex > 2) {
		executeCalibration(imageSize);
	}
	else {
		cout << "\nInsufficient stereo images to calibrate.\n";
	}
}

void StereoCalibration::init(int boardHeight, int boardWidth, bool capImgs, string imgdir, string prefixL, string prefixR, string ext, string imagelist)
{
	dir = imgdir;
	boardSize = Size(boardWidth, boardHeight);
	prefixLeft = prefixL;
	prefixRight = prefixR;
	postfix = ext;
	captureImages = capImgs;
	imagelist_file_name = imagelist;

	namedWindow("Left Image");
	namedWindow("Right Image");
}