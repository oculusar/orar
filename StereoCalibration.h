#pragma once

#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <C/FlyCapture2_C.h>


using namespace cv;
using namespace std;

class StereoCalibration
{
	enum Modes { DETECTING, CAPTURING, CALIBRATING };
	Modes mode = DETECTING;
	int stereoPairIndex = 0, cornerImageIndex = 0;
	int goIn = 1;
	Mat _leftOri, _rightOri;
	int64 prevTickCount = 0;
	int noOfStereoPairs = 0;
	vector<cv::Point2f> cornersLeft, cornersRight;
	vector<vector<cv::Point2f> > cameraImagePoints[2];
	cv::Size boardSize;

	string prefixLeft;
	string prefixRight;
	string postfix;
	string dir;
	string imagelist_file_name;

	bool captureImages = false;
	cv::Size inputSize;
public:
	typedef struct {
		float fx;
		float fy;
		float cx;
		float cy;
		float k1;
		float k2;
		float r11;
		float r21;
		float r31;
		float r12;
		float r22;
		float r32;
		float r13;
		float r23;
		float r33;
		Mat rot;
	} calib_data;

	bool checkNewFrames(fc2Image &img1, fc2Image &img2);
	void calibrateFromSavedImages();
	void startCalibration();
	void init(int boardHeight, int boardWidth, bool capImgs, string imgdir, string prefixL, string prefixR, string ext, string listname);
	static calib_data* readCalibrationData();
	string getNumberOfConfiguredImages() const;
private:
	void executeCalibration(cv::Size);
	bool findChessboardCornersAndDraw(Mat, Mat);
	void saveImages(Mat, Mat, int, bool);
	Mat displayMode(Mat) const;
	void displayImages() const;
	static bool readStringList(const string& filename, vector<string>& l);
	static bool writeStringList(const string& filename, vector<string> l);
};