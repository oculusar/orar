#pragma once

#include <GL/CAPI_GLE.h>
#include "Win32_GLAppUtil.h"
#include "Font.h"
#include "Extras/OVR_Math.h"

class FontRenderer
{

public:
	FontRenderer();
	~FontRenderer();

	void render(GLuint texId);
	void setText(string text, float posX, float posY, float textSize);
private:


	const char* FragShaderSrc =
		"#version 150\n"
		"uniform sampler2D _MainTex;\n"
		"in		 vec4 oColor;\n"
		"in		 vec2 oTexCoord;\n"
		"out     vec4      FragColor;\n"
		"void main()\n"
		"{\n"
		"   vec4 finalColor = oColor;\n"
		"   finalColor.a *= texture(Texture0, oTexCoord).r;\n"
		// Blend state expects premultiplied alpha
		"   finalColor.rgb *= finalColor.a;\n"
		"   FragColor = finalColor;\n"
		"}\n";

	const char* VertexShaderSrc =
		"#version 150\n"
		"in      vec2 Position;\n"
		"in		 vec4 Color;\n"
		"in      vec2 TexCoord;\n"
		"out	 vec4 oColor;\n"
		"out	 vec2 oTexCoord;\n"
		"void main()\n"
		"{\n"
		"   gl_Position = vec4(Position,0,1);\n"
		"   oTexCoord = TexCoord;\n"
		"   oColor = Color;\n"
		"}\n";

	GLuint program;
	VertexBuffer* vbuffer;

	static GLuint CreateShader(GLenum type, const GLchar* src);
	static float measureText(const Font* font, const char* str, float size, float strsize[2], const size_t charRange[2], Vector2f charRangeRect[2]);
	void fillVertexBuffer(const Font* font, const char* str, float x, float y, float size);
};

