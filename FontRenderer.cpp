#include "FontRenderer.h"
#include <vector>

#include "Kernel/OVR_Std.h"


#ifndef OVR_DEBUG_LOG
#define OVR_DEBUG_LOG(x)
#endif

struct Vertex
{
	Vector2f  Pos;
	Vector4f  Color;
	float     U, V;
};

FontRenderer::FontRenderer()
{
	GLuint    vshader = CreateShader(GL_VERTEX_SHADER, VertexShaderSrc);
	GLuint    fshader = CreateShader(GL_FRAGMENT_SHADER, FragShaderSrc);

	program = glCreateProgram();

	glAttachShader(program, vshader);
	glAttachShader(program, fshader);

	glLinkProgram(program);

	glDetachShader(program, vshader);
	glDetachShader(program, fshader);

	//Errohandling ---
	GLint r;
	glGetProgramiv(program, GL_LINK_STATUS, (int *)&r);
	if (r == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		//The maxLength includes the NULL character
		vector<GLchar> infoLog(maxLength);
		glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);

		//We don't need the program anymore.
		glDeleteProgram(program);
		//Don't leak shaders either.
		glDeleteShader(vshader);
		glDeleteShader(fshader);

		OVR_DEBUG_LOG(("Linking shaders failed: %s\n", infoLog));
	}
}


FontRenderer::~FontRenderer()
{
	delete vbuffer; vbuffer = nullptr;
	if (program)
	{
		glDeleteProgram(program);
		program = 0;
	}
}

void FontRenderer::render(GLuint texId)
{
	glUseProgram(program);

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

	glUniform1i(glGetUniformLocation(program, "_MainTex"), 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texId);

	glBindBuffer(GL_ARRAY_BUFFER, vbuffer->buffer);

	GLuint colLoc = glGetAttribLocation(program, "Color");
	GLuint posLoc = glGetAttribLocation(program, "Position");
	GLuint uvLoc = glGetAttribLocation(program, "TexCoord");

	glEnableVertexAttribArray(colLoc);
	glEnableVertexAttribArray(posLoc);
	glEnableVertexAttribArray(uvLoc);

	glVertexAttribPointer(colLoc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)OVR_OFFSETOF(Vertex, Color));
	glVertexAttribPointer(posLoc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)OVR_OFFSETOF(Vertex, Pos));
	glVertexAttribPointer(uvLoc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)OVR_OFFSETOF(Vertex, U));

	glDrawArrays(GL_TRIANGLES, 0, 4);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glDisable(GL_BLEND);

	glUseProgram(0);
}

void FontRenderer::setText(string text, float posX, float posY, float textSize)
{
	const char* txt = text.c_str();

	float size[2] = { 0.0f, 0.0f };

	size_t   selection[2] = { 0, 0 };
	Vector2f selectionRect[2];

	fillVertexBuffer(&DejaVu, txt, posX, posY, textSize);
}

float FontRenderer::measureText(const Font* font, const char* str, float size, float strsize[2], const size_t charRange[2], Vector2f charRangeRect[2])
{
	size_t length = strlen(str);
	float w = 0;
	float xp = 0;
	float yp = 0;

	for (size_t i = 0; i < length; i++)
	{
		if (str[i] == '\n')
		{
			yp += font->lineheight;
			if (xp > w)
			{
				w = xp;
			}
			xp = 0;
			continue;
		}

		// Record top-left charRange rectangle coordinate.
		if (charRange && charRangeRect && (i == charRange[0]))
			charRangeRect[0] = Vector2f(xp, yp);

		// Tab followed by a numbers sets position to specified offset.
		if (str[i] == '\t')
		{
			char *p = 0;
			float tabPixels = (float)OVR_strtoq(str + i + 1, &p, 10);
			i += p - (str + i + 1);
			xp = tabPixels;
		}
		else
		{
			const Font::Char* ch = &font->chars[(int)str[i]];
			xp += ch->advance;
		}

		// End of character range.
		// Store 'xp' after advance, yp will advance later.
		if (charRange && charRangeRect && (i == charRange[1]))
			charRangeRect[1] = Vector2f(xp, yp);
	}

	if (xp > w)
	{
		w = xp;
	}

	float scale = (size / font->lineheight);

	if (strsize)
	{
		strsize[0] = scale * w;
		strsize[1] = scale * (yp + font->lineheight);
	}

	if (charRange && charRangeRect)
	{
		// Selection rectangle ends in the bottom.
		charRangeRect[1].y += font->lineheight;
		charRangeRect[0] *= scale;
		charRangeRect[1] *= scale;
	}

	return (size / font->lineheight) * w;
}

void FontRenderer::fillVertexBuffer(const Font* font, const char* str, float x, float y, float size)
{
	size_t length = strlen(str);

	// Do not attempt to render if we have an empty string.
	if (length == 0) { return; }

	Vertex* vertices = new Vertex[length * 6];

	Matrix4f m = Matrix4f(size / font->lineheight, 0, 0, 0,
		0, size / font->lineheight, 0, 0,
		0, 0, 0, 0,
		x, y, 0, 1).Transposed();

	float xp = 0, yp = (float)font->ascent;
	int   ivertex = 0;

	for (size_t i = 0; i < length; i++)
	{
		if (str[i] == '\n')
		{
			yp += font->lineheight;
			xp = 0;
			continue;
		}
		// Tab followed by a numbers sets position to specified offset.
		if (str[i] == '\t')
		{
			char *p = 0;
			float tabPixels = (float)OVR_strtoq(str + i + 1, &p, 10);
			i += p - (str + i + 1);
			xp = tabPixels;
			continue;
		}

		const Font::Char* ch = &font->chars[(int)str[i]];
		Vertex* chv = &vertices[ivertex];
		float fx = xp + ch->x;
		float fy = yp - ch->y;
		float cx = font->twidth * (ch->u2 - ch->u1);
		float cy = font->theight * (ch->v2 - ch->v1);

		const Vector4f& c = Vector4f(255, 255, 255, 255);

		chv[0] = Vertex{ Vector2f(fx, fy), c, ch->u1, ch->v1 };
		chv[1] = Vertex{ Vector2f(fx + cx, fy), c, ch->u2, ch->v1 };
		chv[2] = Vertex{ Vector2f(fx + cx, cy + fy), c, ch->u2, ch->v2 };
		chv[3] = Vertex{ Vector2f(fx, fy), c, ch->u1, ch->v1 };
		chv[4] = Vertex{ Vector2f(fx + cx, cy + fy), c, ch->u2, ch->v2 };
		chv[5] = Vertex{ Vector2f(fx, fy + cy), c, ch->u1, ch->v2 };
		ivertex += 6;

		xp += ch->advance;
	}

	vbuffer = new VertexBuffer(vertices, ivertex * sizeof(Vertex));
}


GLuint FontRenderer::CreateShader(GLenum type, const GLchar* src)
{
	GLuint shader = glCreateShader(type);

	glShaderSource(shader, 1, &src, NULL);
	glCompileShader(shader);

	GLint r;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &r);
	if (!r)
	{
		GLchar msg[1024];
		glGetShaderInfoLog(shader, sizeof(msg), 0, msg);
		if (msg[0]) {
			OVR_DEBUG_LOG(("Compiling shader failed: %s\n", msg));
		}
		return 0;
	}

	return shader;
}

