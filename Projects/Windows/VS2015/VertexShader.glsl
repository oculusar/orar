#version 150
in      vec2 TexCoord;
in      vec2 Position;
out     vec2 oTexCoord;

void main()
{
   oTexCoord   = TexCoord;
   gl_Position = vec4(Position,0,1);
}