#version 150

uniform sampler2D _MainTex;
in      vec2      oTexCoord;
out     vec4      FragColor;

void main()
{
   FragColor = texture2D(_MainTex, oTexCoord);
}