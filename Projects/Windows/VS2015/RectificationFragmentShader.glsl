#version 150

uniform sampler2D _MainTex;
uniform float _FX;
uniform float _FY;
uniform float _CX;
uniform float _CY;
uniform vec2 _FlycamSize;

uniform float _K1;
uniform float _K2;

uniform float _R11;
uniform float _R12;
uniform float _R13;

uniform float _R21;
uniform float _R22;
uniform float _R23;

uniform float _R31;
uniform float _R32;
uniform float _R33;

uniform float _OR_F;
uniform float _OR_CX;
uniform float _OR_CY;
uniform vec2 _OR_Size;

out vec4 FragColor;
in vec2 oTexCoord;

void main () {
	float xs = (oTexCoord.x * _OR_Size.x - _OR_CX);
	float ys = (oTexCoord.y * _OR_Size.y - _OR_CY);
	float zs = _OR_F;
	
	float xt = _R11 * xs + _R21 * ys + _R31 * zs;
	float yt = _R12 * xs + _R22 * ys + _R32 * zs;
	float zt = _R13 * xs + _R23 * ys + _R33 * zs;
	
	float xrs = xt / zt;
	float yrs = yt / zt;
	
	float r2 = xrs * xrs + yrs * yrs;
	float r4 = r2 * r2;
	
	float xrss = xrs * (1 + _K1 * r2 + _K2 * r4);
	float yrss = yrs * (1 + _K1 * r2 + _K2 * r4);
	
	float u = ((_FX * xrss + _CX) / _FlycamSize.x);
	float v = ((_FY * yrss + _CY) /  _FlycamSize.y);
	vec2 coords = vec2(u, v);

	if(coords.y < 0.005) {
		coords.y=0.005;	
	}

	FragColor = texture2D(_MainTex, coords);
}