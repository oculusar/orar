#pragma once

#include <GL/CAPI_GLE.h>
#include "Win32_GLAppUtil.h"
#include "StereoCalibration.h"
#include "ShaderProgram.h"

using namespace OVR;

class ScreenQuad
{
public:
	ScreenQuad();
	~ScreenQuad();

	void render(GLuint texId, bool);
	void updateRectifyData(StereoCalibration::calib_data* ,bool, int, int, int, int, float fov);
private:
	//Variables
	GLuint program;
	VertexBuffer * vbuffer;
	ShaderProgram simpleShader;
	ShaderProgram rectifiedShader;
};
