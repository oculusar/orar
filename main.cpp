/*****************************************************************************

Filename    :   main.cpp
Content     :   Simple minimal VR demo
Created     :   December 1, 2014
Author      :   Tom Heath
Copyright   :   Copyright 2012 Oculus, Inc. All Rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

/*****************************************************************************/
/// This sample has not yet been fully assimiliated into the framework
/// and also the GL support is not quite fully there yet, hence the VR
/// is not that great!

#define FLYCAM
#define CALIBRATION

#include "ScreenQuad.h"
#include <process.h>
#include <iostream>
#include "StereoCalibration.h"
#include <boost/lockfree/spsc_queue.hpp>
#include <boost/circular_buffer.hpp>
#include "linmath.h"

#ifdef FLYCAM

#include "Flycam.h"

#endif // FLYCAM

#include "Win32_GLAppUtil.h"

// Include the Oculus SDK
#include "OVR_CAPI_GL.h"
#include "FontRenderer.h"

#if defined(_WIN32)
#include <dxgi.h> // for GetDefaultAdapterLuid
#pragma comment(lib, "dxgi.lib")
#endif

enum States { UNCALIBRATED, CALIBRATING, CALIBRATED, LOADING, CALCULATING, NONE };
string states_desc[] = { "No calibration data found. Press C or L to calibrate", "Calibrating. Captured images: ", "Calibration finished.", "Calibrating from saved images.", "Rectifying the images.", "" };
States curState = UNCALIBRATED;
States newState = UNCALIBRATED;
bool screenQuadDirty = false;

StereoCalibration calibration;
typedef struct {
	int camNum;
	const char* fileName;
} camParam;

string getTextForCurState()
{
	ostringstream str;
	switch (curState)
	{
	case UNCALIBRATED:
		return states_desc[0];
	case CALIBRATING:
		str << states_desc[1];
		str << calibration.getNumberOfConfiguredImages();
		return str.str();
	case CALIBRATED:
		return states_desc[2];
	case LOADING:
		return states_desc[3];
	case CALCULATING:
		return states_desc[4];
	default:
		return states_desc[5];
	}
}

using namespace OVR;

bool camStop = false;

boost::lockfree::spsc_queue<fc2Image, boost::lockfree::capacity<3> > tripple_bufferLeft;
boost::lockfree::spsc_queue<fc2Image, boost::lockfree::capacity<3> > tripple_bufferRight;
const int flycamImageWidth = 1392;
const int flycamImageHeight = 1552;
// RiftAR: Define our own functions


// RiftAR: Define our own Flycam dependend functions.
fc2Image imageLeft;
fc2Image imageRight;

#ifdef FLYCAM

// RiftAR: Define both camera images
unsigned int numCameras = -1;

#endif // FLYCAM

static ovrGraphicsLuid GetDefaultAdapterLuid()
{
	ovrGraphicsLuid luid = ovrGraphicsLuid();

#if defined(_WIN32)
	IDXGIFactory* factory = nullptr;

	if (SUCCEEDED(CreateDXGIFactory(IID_PPV_ARGS(&factory))))
	{
		IDXGIAdapter* adapter = nullptr;

		if (SUCCEEDED(factory->EnumAdapters(0, &adapter)))
		{
			DXGI_ADAPTER_DESC desc;

			adapter->GetDesc(&desc);
			memcpy(&luid, &desc.AdapterLuid, sizeof(luid));
			adapter->Release();
		}

		factory->Release();
	}
#endif

	return luid;
}



Mat quaternionToRotMatrix(float x, float y, float z, float w)
{
	Mat toRet(3, 3, DataType<double>::type);

	toRet.at<double>(0, 0) = 1 - 2 * y * y - 2 * z * z;
	toRet.at<double>(0, 1) = 2 * x * y + 2 * z * w;
	toRet.at<double>(0, 2) = 2 * x * z - 2 * y * w;

	toRet.at<double>(1, 0) = 2 * x * y - 2 * z * w;
	toRet.at<double>(1, 1) = 1 - 2 * x * x - 2 * z * z;
	toRet.at<double>(1, 2) = 2 * y * z + 2 * x * w;

	toRet.at<double>(2, 0) = 2 * x * z + 2 * y * w;
	toRet.at<double>(2, 1) = 2 * y * z - 2 * x * w;
	toRet.at<double>(2, 2) = 1 - 2 * x * x - 2 * y * y;

	return toRet;
}

//TODO move to header file
void matrixToQuaternion(quat toRet, Mat mat)
{
	//TODO check if the matrix is 3x3

	float trace = mat.at<double>(0,0) + mat.at<double>(1,1) + mat.at<double>(2,2); // I removed + 1.0f; see discussion with Ethan
	if (trace > 0) {// I changed M_EPSILON to 0
		float s = 0.5f / sqrtf(trace + 1.0f);
		toRet[3] = 0.25f / s; //w
		toRet[0]= (float)(mat.at<double>(2,1) - mat.at<double>(1,2)) * s; //x
		toRet[1] = (float)(mat.at<double>(0,2) - mat.at<double>(2,0)) * s; //y
		toRet[2] = (float)(mat.at<double>(1,0) - mat.at<double>(0,1)) * s; //z
	}
	else {
		if (mat.at<double>(0,0) > mat.at<double>(1,1) && mat.at<double>(0,0) > mat.at<double>(2,2)) {
			double s = 2.0f * sqrtf(1.0f + mat.at<double>(0,0) - mat.at<double>(1,1) - mat.at<double>(2,2));
			toRet[3] = (float)(mat.at<double>(2,1) - mat.at<double>(1,2)) / s;
			toRet[0] = 0.25f * s;
			toRet[1] = (float)(mat.at<double>(0,1) + mat.at<double>(1,0)) / s;
			toRet[2] = (float)(mat.at<double>(0,2) + mat.at<double>(2,0)) / s;
		}
		else if (mat.at<double>(1,1) > mat.at<double>(2,2)) {
			double s = 2.0f * sqrtf(1.0f + mat.at<double>(1,1) - mat.at<double>(0,0) - mat.at<double>(2,2));
			toRet[3] = (float)(mat.at<double>(0,2) - mat.at<double>(2,0)) / s;
			toRet[0] = (float)(mat.at<double>(0,1) + mat.at<double>(1,0)) / s;
			toRet[1] = 0.25f * s;
			toRet[2] = (float)(mat.at<double>(1,2) + mat.at<double>(2,1)) / s;
		}
		else {
			double s = 2.0f * sqrtf(1.0f + mat.at<double>(2,2) - mat.at<double>(0,0) - mat.at<double>(1,1));
			toRet[3] = (float) (mat.at<double>(1,0) - mat.at<double>(0,1)) / s;
			toRet[0] = (float) (mat.at<double>(0,2) + mat.at<double>(2,0)) / s;
			toRet[1] = (float) (mat.at<double>(1,2) + mat.at<double>(2,1)) / s;
			toRet[2] = 0.25f * s;
		}
	}
}

void updateRotation(StereoCalibration::calib_data* dat, quat diff, int i)
{
	quat q1;
	quat qr1;
	matrixToQuaternion(qr1, dat[i].rot);
	quat_mul(q1, diff, qr1);
	Mat rot1 = quaternionToRotMatrix(q1[0], q1[1], q1[2], q1[3]);


	dat[i].r11 = (float)rot1.at<double>(0, 0);
	dat[i].r12 = (float)rot1.at<double>(0, 1);
	dat[i].r13 = (float)rot1.at<double>(0, 2);

	dat[i].r21 = (float)rot1.at<double>(1, 0);
	dat[i].r22 = (float)rot1.at<double>(1, 1);
	dat[i].r23 = (float)rot1.at<double>(1, 2);

	dat[i].r31 = (float)rot1.at<double>(2, 0);
	dat[i].r32 = (float)rot1.at<double>(2, 1);
	dat[i].r33 = (float)rot1.at<double>(2, 2);
}

ScreenQuad* screenQuad;
FontRenderer* fontRenderer;

typedef struct {
	double timestamp;
	ovrPosef pose;
} timeStampPose;

// Return true to retry later (e.g. after display lost)
static bool MainLoop(bool retryCreate)
{
	TextureBuffer * objectRenderTexture[2] = { nullptr,nullptr };
	DepthBuffer   * objectDepthBuffer[2] = { nullptr, nullptr };
	boost::circular_buffer<timeStampPose> poseBuffer(15);

	TextureBuffer * eyeRenderTexture[2] = { nullptr, nullptr };
	DepthBuffer   * eyeDepthBuffer[2] = { nullptr, nullptr };
	ovrMirrorTexture mirrorTexture = nullptr;
	GLuint          mirrorFBO = 0;
	Scene         * roomScene = nullptr;
	bool isVisible = true;
	long long frameIndex = 0;

	ovrSession session;
	ovrGraphicsLuid luid;

	ovrResult result = ovr_Create(&session, &luid);
	if (!OVR_SUCCESS(result))
		return retryCreate;

	if (memcmp(&luid, &GetDefaultAdapterLuid(), sizeof(luid))) // If luid that the Rift is on is not the default adapter LUID...
	{
		VALIDATE(false, "OpenGL supports only the default graphics adapter.");
	}

	ovrHmdDesc hmdDesc = ovr_GetHmdDesc(session);

	// Setup Window and Graphics
	// Note: the mirror window can be any size, for this sample we use 1/2 the HMD resolution
	ovrSizei windowSize = { hmdDesc.Resolution.w / 2, hmdDesc.Resolution.h / 2 };
	if (!Platform.InitDevice(windowSize.w, windowSize.h, reinterpret_cast<LUID*>(&luid)))
		goto Done;

	// Make eye render buffers
	for (int eye = 0; eye < 2; ++eye)
	{
		//Initialize Eyetextures
		ovrSizei idealTextureSize = ovr_GetFovTextureSize(session, ovrEyeType(eye), hmdDesc.DefaultEyeFov[eye], 1);
		eyeRenderTexture[eye] = new TextureBuffer(session, true, true, idealTextureSize, 1, NULL, 1);
		eyeDepthBuffer[eye] = new DepthBuffer(eyeRenderTexture[eye]->GetSize(), 0);

		if (!eyeRenderTexture[eye]->TextureChain)
		{
			if (retryCreate) goto Done;
			VALIDATE(false, "Failed to create texture.");
		}


		//Initialize ObjectTextures
		//Initialize Eyetextures
		objectRenderTexture[eye] = new TextureBuffer(session, true, true, idealTextureSize, 1, NULL, 1);
		objectDepthBuffer[eye] = new DepthBuffer(objectRenderTexture[eye]->GetSize(), 0);

		if (!objectRenderTexture[eye]->TextureChain)
		{
			if (retryCreate) goto Done;
			VALIDATE(false, "Failed to create texture.");
		}
	}

	ovrMirrorTextureDesc desc;
	memset(&desc, 0, sizeof(desc));
	desc.Width = windowSize.w;
	desc.Height = windowSize.h;
	desc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;

	// Create mirror texture and an FBO used to copy mirror texture to back buffer
	result = ovr_CreateMirrorTextureGL(session, &desc, &mirrorTexture);
	if (!OVR_SUCCESS(result))
	{
		if (retryCreate) goto Done;
		VALIDATE(false, "Failed to create mirror texture.");
	}

	// Configure the mirror read buffer
	GLuint texId;
	ovr_GetMirrorTextureBufferGL(session, mirrorTexture, &texId);

	glGenFramebuffers(1, &mirrorFBO);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, mirrorFBO);
	glFramebufferTexture2D(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texId, 0);
	glFramebufferRenderbuffer(GL_READ_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

	// Turn off vsync to let the compositor do its magic
	wglSwapIntervalEXT(0);

	// Make scene - can simplify further if needed
	roomScene = new Scene(false);

	// FloorLevel will give tracking poses where the floor height is 0
	ovr_SetTrackingOriginType(session, ovrTrackingOrigin_FloorLevel);

	// Initialize textures
	DWORD* left_tex_pixels = new DWORD[flycamImageWidth * flycamImageHeight];
	left_tex_pixels[0] = 0xffffffff;
	TextureBuffer* left_texture = new TextureBuffer(nullptr, false, false, Sizei(flycamImageWidth, flycamImageHeight), 4, (unsigned char *)left_tex_pixels, 1);
	
	DWORD* right_tex_pixels = new DWORD[flycamImageWidth * flycamImageHeight];
	right_tex_pixels[0] = 0xffffffff;
	TextureBuffer* right_texture = new TextureBuffer(nullptr, false, false, Sizei(flycamImageWidth, flycamImageHeight), 4, (unsigned char *)right_tex_pixels, 1);

	unsigned char* img_data_left = (unsigned char*)0;
	unsigned char* img_data_right = (unsigned char*)0;

	screenQuad = new ScreenQuad();

	StereoCalibration::calib_data* data = calibration.readCalibrationData();
	if(data == nullptr)
	{
		newState = UNCALIBRATED;
	} else
	{
		newState = CALIBRATED;
	}

	fontRenderer = new FontRenderer();

	
	long long lastFlycamLeftTimestamp = -1;
	long long lastFlycamRightTimestamp = -1;
	long long firstFlycamTimestamp = -1;
	double firstORTimestamp = -1;
	long long* lastFlycamTs;
	long long fts = -1;

	// Main loop
	while (Platform.HandleMessages())
	{
		// Keyboard inputs to adjust player orientation
		static float Yaw(3.141592f);
		if (Platform.Key[VK_LEFT])  Yaw += 0.02f;
		if (Platform.Key[VK_RIGHT]) Yaw -= 0.02f;

		// Keyboard inputs to adjust player position
		static Vector3f Pos2(0.0f, 0.0f, -5.0f);
		if (Platform.Key['W'] || Platform.Key[VK_UP])     Pos2 += Matrix4f::RotationY(Yaw).Transform(Vector3f(0, 0, -0.05f));
		if (Platform.Key['S'] || Platform.Key[VK_DOWN])   Pos2 += Matrix4f::RotationY(Yaw).Transform(Vector3f(0, 0, +0.05f));
		if (Platform.Key['D'])                          Pos2 += Matrix4f::RotationY(Yaw).Transform(Vector3f(+0.05f, 0, 0));
		if (Platform.Key['A'])                          Pos2 += Matrix4f::RotationY(Yaw).Transform(Vector3f(-0.05f, 0, 0));
#ifdef CALIBRATION
		if (Platform.Key['C']) {
			newState = CALIBRATING;
		}
		if (Platform.Key['F']) {
			newState = CALCULATING;
		}
		if (Platform.Key['L']) {
			newState = LOADING;
		}
		if (Platform.Key['Q'])
		{
			newState = UNCALIBRATED;
		}
		if (Platform.Key['E'])
		{
			StereoCalibration::calib_data* data = calibration.readCalibrationData();
			if (data == nullptr)
			{
				newState = UNCALIBRATED;
			}
			else
			{
				newState = CALIBRATED;
			}
		}
#endif

		double displayMidpointSeconds = ovr_GetPredictedDisplayTime(session, 0);

		ovrTrackingState ts = ovr_GetTrackingState(session, displayMidpointSeconds, ovrTrue);
		ovrPosef curPose;
		bool poseAquired = false;
		if (ts.StatusFlags & (ovrStatus_OrientationTracked | ovrStatus_PositionTracked))
		{
			curPose = ts.HeadPose.ThePose;
			if(displayMidpointSeconds != 0) {
				poseBuffer.push_back(timeStampPose{ displayMidpointSeconds  , curPose });
				if(firstORTimestamp == -1.0f)
				{
					firstORTimestamp = displayMidpointSeconds;
				}
			}
		}

		// Animate the cube
		static float cubeClock = 0;
		roomScene->Models[0]->Pos = Vector3f(9 * (float)sin(cubeClock), 3, 9 * (float)cos(cubeClock += 0.015f));

#ifdef FLYCAM
		bool success = tripple_bufferLeft.pop(imageLeft);
		bool success2 = tripple_bufferRight.pop(imageRight);
		
		if (success) {
			fc2GetImageData(&imageLeft, &img_data_left);
			left_texture->SwapTextures(img_data_left);
		}

		if(success2) {
			fc2GetImageData(&imageRight, &img_data_right);
			right_texture->SwapTextures(img_data_right);
		}

#endif // FLYCAM

		

		// Call ovr_GetRenderDesc each frame to get the ovrEyeRenderDesc, as the returned values (e.g. HmdToEyeOffset) may change at runtime.
		ovrEyeRenderDesc eyeRenderDesc[2];
		eyeRenderDesc[0] = ovr_GetRenderDesc(session, ovrEye_Left, hmdDesc.DefaultEyeFov[0]);
		eyeRenderDesc[1] = ovr_GetRenderDesc(session, ovrEye_Right, hmdDesc.DefaultEyeFov[1]);

		// Get eye poses, feeding in correct IPD offset
		ovrPosef                  EyeRenderPose[2];
		ovrVector3f               HmdToEyeOffset[2] = { eyeRenderDesc[0].HmdToEyeOffset,
														eyeRenderDesc[1].HmdToEyeOffset };

		double sensorSampleTime;    // sensorSampleTime is fed into the layer later
		ovr_GetEyePoses(session, frameIndex, ovrTrue, HmdToEyeOffset, EyeRenderPose, &sensorSampleTime);

		//TEST: OUR IMPLENTATION OF GETING A HEAD POSE.
		if (isVisible)
		{
			for (int eye = 0; eye < 2; ++eye)
			{

				if ((success && eye == 0) || (success2 && eye == 1) ) //Only if we could grab a new image
				{
					fc2Image* im;
					if (eye == 0) {
						im = &imageLeft;
						lastFlycamTs = &lastFlycamLeftTimestamp;
					}
					else {
						im = &imageRight;
						lastFlycamTs = &lastFlycamRightTimestamp;
					}
					fts = GetTimestamp(im);
					if(firstFlycamTimestamp == -1)
					{
						firstFlycamTimestamp = fts;
					}

					if (*lastFlycamTs != -1 && firstORTimestamp > 0) {
						long long diff = fts - *lastFlycamTs;
						double diffToUse = diff; //Now we have a diff which should be in the same range as 

						ovrPosef lastPose;
						boolean found = false;
						for(auto it = poseBuffer.rbegin(); it != poseBuffer.rend();++it)
						{
							double tsCur = it->timestamp;
							double tsLast = (poseBuffer.begin())->timestamp;
							double ovrDiff = tsCur - tsLast;
							if(diffToUse < ovrDiff) //We found a suitable rotation
							{
								lastPose = it->pose;
								found = true;
								break;
							}
						}
						if (found) {
							quat old = { lastPose.Orientation.x, lastPose.Orientation.y, lastPose.Orientation.z, lastPose.Orientation.w };
							quat cur = { curPose.Orientation.x, curPose.Orientation.y, curPose.Orientation.z, curPose.Orientation.w }; //TODO check if definied correctly
							quat invOld;
							quat_conj(invOld, old); //As these are rotation quaternions the length should be 1. So as the inverse of a quaternion is the conjugate divided by the norm, a conjugate should be equal to the inverse

							quat diffRot;
							quat_mul(diffRot, cur, invOld);
							updateRotation(data, diffRot, eye); //TODO Check if eye is the correct index
						}
					}

				}
				*lastFlycamTs = fts;
				// Switch to eye render target
				eyeRenderTexture[eye]->SetAndClearRenderSurface(eyeDepthBuffer[eye]);
				
				ovrSizei idealTextureSize = ovr_GetFovTextureSize(session, ovrEyeType(eye), hmdDesc.DefaultEyeFov[eye], 1);
				//fontRenderer->setText(getTextForCurState(), idealTextureSize.w / 2.f, idealTextureSize.h / 2.f, 22.f);
				if (eye == 0) {
					if (curState == CALIBRATED) {
						screenQuad->updateRectifyData(data, true, flycamImageWidth, flycamImageHeight, idealTextureSize.w, idealTextureSize.h, 106.19f); //TODO or do we use 95.06 (horizontal fov)
					}
					screenQuad->render(left_texture->texId, curState == CALIBRATED);
					//TODO get the fov from the OR API
					//fontRenderer->render(rightFill->texture->texId);
				}
				else {
					if (curState == CALIBRATED) { //Don't set these every time. Only when dirty (rotation changed + first time)
						screenQuad->updateRectifyData(data, false, flycamImageWidth, flycamImageHeight, idealTextureSize.w, idealTextureSize.h, 106.19f); //TODO maybe we need the flycam image size
					}
					screenQuad->render(right_texture->texId, curState == CALIBRATED);
					//fontRenderer->render(leftFill->texture->texId);
				}

				// Avoids an error when calling SetAndClearRenderSurface during next iteration.
				// Without this, during the next while loop iteration SetAndClearRenderSurface
				// would bind a framebuffer with an invalid COLOR_ATTACHMENT0 because the texture ID
				// associated with COLOR_ATTACHMENT0 had been unlocked by calling wglDXUnlockObjectsNV.
				eyeRenderTexture[eye]->UnsetRenderSurface();

				// Commit changes to the textures so they get picked up frame
				eyeRenderTexture[eye]->Commit();
			}
		}


		if (isVisible)
		{
			for (int eye = 0; eye < 2; ++eye)
			{
				// Switch to eye render target
				objectRenderTexture[eye]->SetAndClearRenderSurface(objectDepthBuffer[eye]);

				//// Get view and projection matrices
				Matrix4f rollPitchYaw = Matrix4f::RotationY(Yaw);
				Matrix4f finalRollPitchYaw = rollPitchYaw * Matrix4f(EyeRenderPose[eye].Orientation);
				Vector3f finalUp = finalRollPitchYaw.Transform(Vector3f(0, 1, 0));
				Vector3f finalForward = finalRollPitchYaw.Transform(Vector3f(0, 0, -1));
				Vector3f shiftedEyePos = Pos2 + rollPitchYaw.Transform(EyeRenderPose[eye].Position);

				Matrix4f view = Matrix4f::LookAtRH(shiftedEyePos, shiftedEyePos + finalForward, finalUp);
				Matrix4f proj = ovrMatrix4f_Projection(hmdDesc.DefaultEyeFov[eye], 0.2f, 1000.0f, ovrProjection_None);

				//// Render world
				roomScene->Render(view, proj);

				// Avoids an error when calling SetAndClearRenderSurface during next iteration.
				// Without this, during the next while loop iteration SetAndClearRenderSurface
				// would bind a framebuffer with an invalid COLOR_ATTACHMENT0 because the texture ID
				// associated with COLOR_ATTACHMENT0 had been unlocked by calling wglDXUnlockObjectsNV.
				objectRenderTexture[eye]->UnsetRenderSurface();

				// Commit changes to the textures so they get picked up frame
				objectRenderTexture[eye]->Commit();
			}
		}

		// Do distortion rendering, Present and flush/sync


		//Layer one!
		ovrLayerEyeFov ld;
		ld.Header.Type = ovrLayerType_EyeFov;
		ld.Header.Flags = ovrLayerFlag_TextureOriginAtBottomLeft;   // Because OpenGL.

		for (int eye = 0; eye < 2; ++eye)
		{
			ld.ColorTexture[eye] = eyeRenderTexture[eye]->TextureChain;
			ld.Viewport[eye] = Recti(eyeRenderTexture[eye]->GetSize());
			ld.Fov[eye] = hmdDesc.DefaultEyeFov[eye];
			ld.RenderPose[eye] = EyeRenderPose[eye];
			ld.SensorSampleTime = sensorSampleTime;
		}

		//Layer two!
		ovrLayerEyeFov objLayer;
		objLayer.Header.Type = ovrLayerType_EyeFov;
		objLayer.Header.Flags = ovrLayerFlag_TextureOriginAtBottomLeft;   // Because OpenGL.

		for (int eye = 0; eye < 2; ++eye)
		{
			objLayer.ColorTexture[eye] = objectRenderTexture[eye]->TextureChain;
			objLayer.Viewport[eye] = Recti(objectRenderTexture[eye]->GetSize());
			objLayer.Fov[eye] = hmdDesc.DefaultEyeFov[eye];
			objLayer.RenderPose[eye] = EyeRenderPose[eye];
			objLayer.SensorSampleTime = sensorSampleTime;
		}


		ovrLayerHeader* layers[2];
		layers[0] = &ld.Header;
		layers[1] = &objLayer.Header;


		ovrResult result = ovr_SubmitFrame(session, frameIndex, nullptr, layers, 2);


		// exit the rendering loop if submit returns an error, will retry on ovrError_DisplayLost
		if (!OVR_SUCCESS(result))
			goto Done;

		isVisible = (result == ovrSuccess);

		ovrSessionStatus sessionStatus;
		ovr_GetSessionStatus(session, &sessionStatus);
		if (sessionStatus.ShouldQuit)
			goto Done;
		if (sessionStatus.ShouldRecenter)
			ovr_RecenterTrackingOrigin(session);

		// Blit mirror texture to back buffer
		glBindFramebuffer(GL_READ_FRAMEBUFFER, mirrorFBO);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		GLint w = windowSize.w;
		GLint h = windowSize.h;
		glBlitFramebuffer(0, h, w, 0,
			0, 0, w, h,
			GL_COLOR_BUFFER_BIT, GL_NEAREST);
		glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

		SwapBuffers(Platform.hDC);

		frameIndex++;
	}

Done:
	delete roomScene;
	if (mirrorFBO) glDeleteFramebuffers(1, &mirrorFBO);
	if (mirrorTexture) ovr_DestroyMirrorTexture(session, mirrorTexture);

	delete right_texture;
	delete left_texture;
	delete left_tex_pixels;
	delete right_tex_pixels;

	for (int eye = 0; eye < 2; ++eye)
	{
		delete objectDepthBuffer[eye];
		delete objectRenderTexture[eye];
		delete eyeRenderTexture[eye];
		delete eyeDepthBuffer[eye];
	}

	Platform.ReleaseDevice();
	ovr_Destroy(session);

	// Retry on ovrError_DisplayLost
	return retryCreate || OVR_SUCCESS(result) || (result == ovrError_DisplayLost);
}

#ifdef CALIBRATION
void statemachine_main(void *param)
{
	while (true) {
		States evalState = newState;
		switch (evalState)
		{
		case CALCULATING:
			calibration.startCalibration();
			newState = CALIBRATED;
			break;
		case CALIBRATING:
			calibration.checkNewFrames(imageLeft, imageRight);
			break;
		case CALIBRATED:
			if (curState != newState)
				screenQuadDirty = true;
			break;
		case LOADING:
			calibration.calibrateFromSavedImages();
			newState = CALIBRATED;
			break;
		case UNCALIBRATED:
			if (curState != newState)
				screenQuadDirty = true;
			break;
		default:
			break;
		}
		curState = evalState;
	}
}
#endif

#ifdef FLYCAM

// RiftAR: Flycam thread function
void flycam_main(void *param)
{
	fc2Error error;
	fc2Mode mode;
	fc2Context context;
	fc2PGRGuid guid;
	fc2Image rawImage;
	fc2Image convertedImage;
	camParam *camParams = static_cast<camParam*>(param);
	int camNum = camParams->camNum;
	const char* fileName = camParams->fileName;

	fc2Format7ImageSettings settings;

	mode = FC2_MODE_0;

	settings.height = flycamImageHeight;
	settings.width = flycamImageWidth;
	settings.offsetX = 344;
	settings.offsetY = 0;
	settings.mode = mode;
	settings.pixelFormat = FC2_PIXEL_FORMAT_RGB;

	error = fc2CreateContext(&context);
	if (error != FC2_ERROR_OK)
	{
		printf("Error in fc2CreateContext: %d\n", error);
		_endthread();
	}

	error = fc2GetNumOfCameras(context, &numCameras);
	if (error != FC2_ERROR_OK)
	{
		printf("Error in fc2GetNumOfCameras: %d\n", error);
		_endthread();
	}

	if (numCameras == 0)
	{
		// No cameras detected
		printf("No cameras detected.\n");
		_endthread();
	}

	// Get the camera on position cam
	error = fc2GetCameraFromIndex(context, camNum, &guid);
	if (error != FC2_ERROR_OK)
	{
		printf("Error in fc2GetCameraFromIndex: %d\n", error);
		_endthread();
	}

	error = fc2Connect(context, &guid);
	if (error != FC2_ERROR_OK)
	{
		printf("Error in fc2Connect: %d\n", error);
		_endthread();
	}

	PrintCameraInfo(context);

	SetTimeStamping(context, TRUE);

	fc2SetFormat7Configuration(context, &settings, 100);

	error = fc2StartCapture(context);
	if (error != FC2_ERROR_OK)
	{
		printf("Error in fc2StartCapture: %d\n", error);
		_endthread();
	}

	error = fc2CreateImage(&rawImage);
	if (error != FC2_ERROR_OK)
	{
		printf("Error in fc2CreateImage: %d\n", error);
	}

	error = fc2CreateImage(&convertedImage);
	if (error != FC2_ERROR_OK)
	{
		printf("Error in fc2CreateImage: %d\n", error);
	}

	while (!camStop) {

		// Retrieve the image
		error = fc2RetrieveBuffer(context, &rawImage);
		if (error != FC2_ERROR_OK)
		{
			printf("Error in retrieveBuffer: %d\n", error);
		}

		if (error == FC2_ERROR_OK)
		{
			// Convert the final image to RGBA
			error = fc2ConvertImageTo(FC2_PIXEL_FORMAT_RGB8, &rawImage, &convertedImage);
			if (error != FC2_ERROR_OK)
			{
				printf("Error in fc2ConvertImageTo: %d\n", error);
			}
		}

		if (camNum == 0) {
			tripple_bufferLeft.push(convertedImage);
		}
		else {
			tripple_bufferRight.push(convertedImage);
		}

		/*Sleep(1);*/
	}

	// Move cleanup to destructor
	error = fc2DestroyImage(&rawImage);
	if (error != FC2_ERROR_OK)
	{
		printf("Error in fc2DestroyImage: %d\n", error);
	}

	error = fc2DestroyImage(&convertedImage); //TODO destroy every image in buffer
	if (error != FC2_ERROR_OK)
	{
		printf("Error in fc2DestroyImage: %d\n", error);
	}

	error = fc2StopCapture(context);
	if (error != FC2_ERROR_OK)
	{
		printf("Error in fc2StopCapture: %d\n", error);
		_endthread();
	}

	error = fc2DestroyContext(context);
	if (error != FC2_ERROR_OK)
	{
		printf("Error in fc2DestroyContext: %d\n", error);
		_endthread();
	}

	std::cout << "Closing Flycam-Thread " << camNum << std::endl;
	_endthread();
}

#endif // FLYCAM

//-------------------------------------------------------------------------------------
int WINAPI WinMain(HINSTANCE hinst, HINSTANCE, LPSTR, int)
{
	// Initializes LibOVR, and the Rift
	ovrResult result = ovr_Initialize(nullptr);
	VALIDATE(OVR_SUCCESS(result), "Failed to initialize libOVR.");
	VALIDATE(Platform.InitWindow(hinst, L"Oculus Room Tiny (GL)"), "Failed to open window.");
	
#ifdef FLYCAM

	// Set-up the camera parameters
	camParam *camParam0;
	camParam0 = (camParam*)malloc(sizeof(camParam));
	camParam0->camNum = 0;
	camParam0->fileName = "cam0.png";
	camParam *camParam1;
	camParam1 = (camParam*)malloc(sizeof(camParam));
	camParam1->camNum = 1;
	camParam1->fileName = "cam1.png";

	// Run the Flycam
	std::cout << "Starting Flycam-Threads" << std::endl;
	_beginthread(flycam_main, 0, (void*)camParam0);
	_beginthread(flycam_main, 0, (void*)camParam1);

#endif // FLYCAM

#ifdef CALIBRATION
	calibration.init(8, 11, true, ".", "imgl", "imgr", "jpg", "stereo_calib.xml");
	_beginthread(statemachine_main, 0, nullptr);
#endif

	// Run the Rift
	Platform.Run(MainLoop);

	// Shutdown the oculus environment
	ovr_Shutdown();

#ifdef FLYCAM

	// Stop both camera threads
	camStop = true;

	// Clean Up
	free(camParam0);
	free(camParam1);

#endif // FLYCAM

	return(0);
}