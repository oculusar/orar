//=============================================================================
// Copyright � 2008 Point Grey Research, Inc. All Rights Reserved.
//
// This software is the confidential and proprietary information of Point
// Grey Research, Inc. ("Confidential Information").  You shall not
// disclose such Confidential Information and shall use it only in
// accordance with the terms of the license agreement you entered into
// with Point Grey Research, Inc. (PGR).
//
// PGR MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
// SOFTWARE, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE, OR NON-INFRINGEMENT. PGR SHALL NOT BE LIABLE FOR ANY DAMAGES
// SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
// THIS SOFTWARE OR ITS DERIVATIVES.
//=============================================================================

//=============================================================================
// $Id: Flycam.h 244578 2015-08-21 23:30:57Z matthewg $
//=============================================================================

#define FLYCAM

#ifdef FLYCAM

#ifndef FLYCAM_H_
#define FLYCAM_H_

#include <C/FlyCapture2_C.h>

void PrintCameraInfo(fc2Context context);

void SetTimeStamping(fc2Context context, BOOL enableTimeStamp);

void GrabImages(fc2Context context, int numImagesToGrab, const char* name);

long long GetTimestamp(fc2Image* pImage);

#endif // FLYCAM_H_

#endif // FLYCAM