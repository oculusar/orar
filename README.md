# **Readme** #

# 1. Preamble

Oculus Rift AR: Extending Oculus Rift for AR
As individual project for the practical course "Computer Graphics"
At the Eberhardt Karls Universitaet Tuebingen, Germany

This project tries to add two stereo cameras to the Oculus Rift allowing for seethrough AR

# 2. Setup

To compile the project yourself you will need to ...

Download the ...

- Boost Library v1.55 [Download](http://www.boost.org/users/history/version_1_55_0.html)
- Flycam SDK
- Oculus Rift SDK [https://developer.oculus.com/downloads/](https://developer.oculus.com/downloads/)
- OpenCV v3.10 [http://opencv.org/downloads.html](http://opencv.org/downloads.html)

Create environment variables...

- **BOOST_DIR**: Pointing to the root of the Boost directory
- **FLYCAM_DIR**: Pointing to the root of the Flycam directory
- **OPENCV_DIR**: Pointing to the root of the OpenCV directory
- **OVR\_SDK\_DIR**: Pointing to the root of the Oculus SDK directory

# 3. Author

Authors are:

- Adrian Czarkowski
- Mark Boss
- Severin Opel


# 4. Sources

The following sources were used:
    
- OpenCV
- Flycam SDK
- Boost
- Oculus Rift SDK